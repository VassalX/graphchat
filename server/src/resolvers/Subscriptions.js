function newMessageSubscribe(parent, args, context, info) {
    return context.prisma.$subscribe.message({
        mutation_in: ['CREATED']
    }).node();
}

function updateMessageSubscribe(parent, args, context, info) {
    return context.prisma.$subscribe.message({
        mutation_in: ['UPDATED']
    }).node();
}

function newReplySubscribe(parent, args, context, info) {
    return context.prisma.$subscribe.reply({
        mutation_in: ['CREATED']
    }).node();
}

function updateReplySubscribe(parent, args, context, info) {
    return context.prisma.$subscribe.reply({
        mutation_in: ['UPDATED']
    }).node();
}

const newMessage = {
    subscribe: newMessageSubscribe,
    resolve: payload => {
        return payload;
    }
};

const updateMessage = {
    subscribe: updateMessageSubscribe,
    resolve: payload => {
        return payload;
    }
}

const newReply = {
    subscribe: newReplySubscribe,
    resolve: payload => {
        return payload;
    }
};

const updateReply = {
    subscribe: updateReplySubscribe,
    resolve: payload => {
        return payload;
    }
}

module.exports = {
    newMessage,
    updateMessage,
    newReply,
    updateReply
};