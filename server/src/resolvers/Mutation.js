function postMessage(parent, args, context, info) {
    return context.prisma.createMessage({
        likes: 0,
        dislikes: 0,
        text: args.text,
    });
}

async function addLikeMessage(parent, args, context, info) {
    const messageExists = await context.prisma.$exists.message({
        id: args.messageId
    });

    let likes = await context.prisma.message({
        id: args.messageId
    }).likes();

    if (args.like) likes++;
    else likes--;

    if (!messageExists) {
        throw new Error(`Message with ID ${args.messageId} does not exist`);
    }

    return context.prisma.updateMessage({
        where: { id: args.messageId },
        data: {
            likes: likes
        }
    })
}

async function addDislikeMessage(parent, args, context, info) {
    const messageExists = await context.prisma.$exists.message({
        id: args.messageId
    });

    let dislikes = await context.prisma.message({
        id: args.messageId
    }).dislikes();

    if (args.dislike)
        dislikes++;
    else
        dislikes--;

    if (!messageExists) {
        throw new Error(`Message with ID ${args.messageId} does not exist`);
    }

    return context.prisma.updateMessage({
        where: { id: args.messageId },
        data: {
            dislikes: dislikes
        }
    })
}

async function addLikeReply(parent, args, context, info) {
    const replyExists = await context.prisma.$exists.reply({
        id: args.replyId
    });

    let likes = await context.prisma.reply({
        id: args.replyId
    }).likes();

    if (args.like) likes++;
    else likes--;

    if (!replyExists) {
        throw new Error(`Reply with ID ${args.replyId} does not exist`);
    }

    return context.prisma.updateReply({
        where: { id: args.replyId },
        data: {
            likes: likes
        }
    })
}

async function addDislikeReply(parent, args, context, info) {
    const replyExists = await context.prisma.$exists.reply({
        id: args.replyId
    });

    let dislikes = await context.prisma.reply({
        id: args.replyId
    }).dislikes();

    if (args.dislike)
        dislikes++;
    else
        dislikes--;

    if (!replyExists) {
        throw new Error(`Reply with ID ${args.replyIdId} does not exist`);
    }

    return context.prisma.updateReply({
        where: { id: args.replyId },
        data: {
            dislikes: dislikes
        }
    })
}

async function postReply(parent, args, context, info) {
    const messageExists = await context.prisma.$exists.message({
        id: args.messageId
    });

    if (!messageExists) {
        throw new Error(`Message with ID ${args.messageId} does not exist`);
    }

    return context.prisma.createReply({
        text: args.text,
        likes: 0,
        dislikes: 0,
        message: { connect: { id: args.messageId } }
    });
}

module.exports = {
    postMessage,
    postReply,
    addLikeMessage,
    addDislikeMessage,
    addLikeReply,
    addDislikeReply
}