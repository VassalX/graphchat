import React, { useState } from 'react';
import { Mutation } from 'react-apollo';
import { POST_MESSAGE_MUTATION} from '../../../queries';

const MessageForm = props => {
    const [text, setText] = useState('');

    return (
        <div className="form-wrapper">
            <div className="input-wrapper">
                <input type="text" placeholder="Text" value={text} onChange={e => setText(e.target.value)} />
            </div>

            <Mutation
                mutation={POST_MESSAGE_MUTATION}
                variables={{ text: text }}
            >
                {postMutation =>
                    <button className="post-button" onClick={postMutation}>Send</button>
                }
            </Mutation>
        </div>
    );
};

export default MessageForm;