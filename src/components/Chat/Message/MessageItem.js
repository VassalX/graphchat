import React, { useState } from 'react';
import ReplyList from '../Reply/ReplyList';
import { Mutation } from 'react-apollo';
import { ADD_LIKE_MESSAGE, ADD_DISLIKE_MESSAGE } from '../../../queries'

const MessageItem = props => {
  const { id, text, likes, dislikes, replies } = props;
  const [like, setLike] = useState(false);
  const [dislike, setDislike] = useState(false);

  return (
    <div className="message-item">
      <div className="title-wrapper">
        <h2>#{id.slice(-5)}</h2>
        <div>{text}</div>
        <div>
          Likes {likes}
          <Mutation
            mutation={ADD_LIKE_MESSAGE}
          >
            {postMutation =>
              <input
                type="checkbox"
                placeholder="Like"
                name="Like"
                checked={like}
                onChange={e =>{
                  setLike(!like);
                  postMutation({
                    variables: { 
                      messageId: id, 
                      like: !like }
                    })
                  }} />
            }
          </Mutation>
          Dislikes {dislikes}
          <Mutation
            mutation={ADD_DISLIKE_MESSAGE}
          >
            {postMutation =>
              <input
                type="checkbox"
                placeholder="Dislike"
                name="Dislike"
                checked={dislike}
                onChange={e =>{
                  setDislike(!dislike);
                  postMutation({
                    variables: { 
                      messageId: id, 
                      dislike: !dislike }
                    })
                  }} />
            }
          </Mutation>
        </div>
      </div>
      <ReplyList messageId={id} replies={replies} />
    </div>
  );
};

export default MessageItem;