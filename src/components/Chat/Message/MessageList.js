import React from 'react';
import { Query } from 'react-apollo';
import MessageItem from './MessageItem';
import {
    MESSAGE_QUERY,
    NEW_MESSAGES_SUBSCRIPTION,
    UPDATE_MESSAGES_SUBSCRIPTION,
    NEW_REPLIES_SUBSCRIPTION,
    UPDATE_REPLIES_SUBSCRIPTION
} from '../../../queries';

const MessageList = props => {
    const orderBy = 'createdAt_DESC';

    const _subscribeToNewMessages = subscribeToMore => {
        subscribeToMore({
            document: NEW_MESSAGES_SUBSCRIPTION,
            updateQuery: (prev, { subscriptionData }) => {
                if (!subscriptionData.data) return prev;
                const { newMessage } = subscriptionData.data;
                const exists = prev.messages.find(({ id }) => id === newMessage.id);
                if (exists) return prev;
                console.log(prev);

                return Object.assign({}, prev, {
                    messages: [newMessage, ...prev.messages]
                });
            }
        });
    };

    const _subscribeToUpdateMessages = subscribeToMore => {
        subscribeToMore({
            document: UPDATE_MESSAGES_SUBSCRIPTION,
            updateQuery: (prev, { subscriptionData }) => {
                if (!subscriptionData.data) return prev;
                const { newMessage } = subscriptionData.data;
                const index = prev.messages.indexOf(({ id }) => id === newMessage.id);
                if (index < 0) return prev;
                prev.messages[index] = newMessage;
                return Object.assign({}, prev, {
                    messages: prev.messages
                });
            }
        });
    };

    const _subscribeToNewReplies = subscribeToMore => {
        subscribeToMore({
            document: NEW_REPLIES_SUBSCRIPTION,
            updateQuery: (prev, { subscriptionData }) => {
                if (!subscriptionData.data) return prev;
                console.log('new');
                const { newReply } = subscriptionData.data;
                const index = prev.messages.findIndex(({ id }) => id === newReply.message.id);
                if (index < 0) return prev;
                prev.messages[index].replies.push(newReply);
                return Object.assign({}, prev, {
                    messages: prev.messages
                });
            }
        });
    };

    const _subscribeToUpdateReplies = subscribeToMore => {
        subscribeToMore({
            document: UPDATE_REPLIES_SUBSCRIPTION,
            updateQuery: (prev, { subscriptionData }) => {
                if (!subscriptionData.data) return prev;
                console.log('update');
                const { newReply } = subscriptionData.data;
                const messageIndex = prev.messages.findIndex(({ replies }) => {
                    const exists = replies.find(({ id }) => id === newReply.id);
                    return !!exists;
                });
                if (messageIndex < 0) return prev;
                const index = prev.messages[messageIndex].findIndex(({ id }) => 
                    id === newReply.id
                );
                prev.messages[messageIndex].replies[index] = newReply;
                return Object.assign({}, prev, {
                    messages: prev.messages
                });
            }
        });
    };

    return (
        <Query query={MESSAGE_QUERY} variables={{ orderBy }}>
            {({ loading, error, data, subscribeToMore }) => {
                if (loading) return <div>Loading...</div>;
                if (error) return <div>Fetch error</div>;
                _subscribeToNewMessages(subscribeToMore);
                _subscribeToUpdateMessages(subscribeToMore);
                _subscribeToNewReplies(subscribeToMore);
                _subscribeToUpdateReplies(subscribeToMore);

                const { messages } = data;

                return (
                    <div className="message-list">
                        {messages.map(item => {
                            return <MessageItem key={item.id} {...item} />
                        })}
                    </div>
                );
            }}
        </Query>
    );
};

export default MessageList;