import React from 'react';
import MessageForm from './Message/MessageForm';
import MessageList from './Message/MessageList';

const Chat = props => {
    return (
        <div id='chat'>
            <MessageForm/>
            <MessageList/>
        </div>
    );
}

export default Chat;