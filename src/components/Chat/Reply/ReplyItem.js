import React, { useState } from 'react';
import { Mutation } from 'react-apollo';
import { ADD_LIKE_REPLY, ADD_DISLIKE_REPLY } from '../../../queries'

const ReplyItem = props => {
    const { id, text, likes, dislikes } = props;
    const [like, setLike] = useState(false);
    const [dislike, setDislike] = useState(false);

    return (
        <div>
            <div className="title-wrapper">
                <h4>#{id.slice(-5)}</h4>
                <div>{text}</div>
                <div>
                    Likes {likes}
                    <Mutation
                        mutation={ADD_LIKE_REPLY}
                    >
                        {postMutation =>
                            <input
                                type="checkbox"
                                placeholder="Like"
                                name="Like"
                                checked={like}
                                onChange={e => {
                                    setLike(!like);
                                    postMutation({
                                        variables: {
                                            replyId: id,
                                            like: !like
                                        }
                                    })
                                }} />
                        }
                    </Mutation>
                    Dislikes {dislikes}
                    <Mutation
                        mutation={ADD_DISLIKE_REPLY}
                    >
                        {postMutation =>
                            <input
                                type="checkbox"
                                placeholder="Dislike"
                                name="Dislike"
                                checked={dislike}
                                onChange={e => {
                                    setDislike(!dislike);
                                    postMutation({
                                        variables: {
                                            replyId: id,
                                            dislike: !dislike
                                        }
                                    })
                                }} />
                        }
                    </Mutation>
                </div>
            </div>
        </div>
    );
};

export default ReplyItem;