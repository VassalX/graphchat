import React, { useState } from 'react';
import { Mutation } from 'react-apollo';
import { POST_REPLY_MUTATION } from '../../../queries';

const ReplyForm = props => {
    const { messageId } = props;
    const [text, setText] = useState('');

    return (
        <div className="form-wrapper">
            <div className="input-wrapper">
                <input type="text" placeholder="Text" value={text} onChange={e => setText(e.target.value)} />
            </div>
            <Mutation
                mutation={POST_REPLY_MUTATION}
                variables={{ messageId, text }}
            >
                {postMutation =>
                    <button onClick={postMutation}>Send reply</button>
                }
            </Mutation>
        </div>
    );
};

export default ReplyForm;