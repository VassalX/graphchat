import React from 'react';
import { Route, Switch } from 'react-router-dom';
import Chat from './Chat/Chat';
//import './App.scss';

function App() {
  return (
    <div className="App">
      <Switch>
        <Route exact path="/" component={Chat} />
      </Switch>
    </div>
  );
}

export default App;
