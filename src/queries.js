import gql from 'graphql-tag';

export const MESSAGE_QUERY = gql `
  query messageQuery($orderBy: MessageOrderByInput) {
    messages(orderBy: $orderBy) {
      id
      text
      likes
      dislikes
      replies {
        id
        text
        likes
        dislikes
      }
    }
  }
`;

export const POST_MESSAGE_MUTATION = gql `
  mutation PostMutation($text: String!) {
    postMessage(text: $text) {
      id
      text
      likes
      dislikes
      replies {
        id
        text
        likes
        dislikes
      }
    }
  }
`;

export const POST_REPLY_MUTATION = gql `
  mutation PostMutation($messageId: ID!, $text: String!) {
    postReply(messageId: $messageId, text: $text) {
      id
      text
      likes
      dislikes
    }
  }
`;

export const NEW_MESSAGES_SUBSCRIPTION = gql `
  subscription {
    newMessage {
      id
      text
      likes
      dislikes
      replies {
        id
        text
        likes
        dislikes
      }
    }
  }
`;

export const UPDATE_MESSAGES_SUBSCRIPTION = gql `
  subscription {
    updateMessage {
      id
      text
      likes
      dislikes
      replies {
        id
        text
        likes
        dislikes
      }
    }
  }
`;

export const NEW_REPLIES_SUBSCRIPTION = gql `
  subscription {
    newReply {
      id
      text
      likes
      dislikes
      message{
        id
      }
    }
  }
`;

export const UPDATE_REPLIES_SUBSCRIPTION = gql `
  subscription {
    updateReply {
      id
      text
      likes
      dislikes
      message{
        id
      }
    }
  }
`;

export const ADD_LIKE_MESSAGE = gql `
  mutation PostMutation($messageId: ID!, $like: Boolean!) {
    addLikeMessage(messageId: $messageId, like: $like) {
      id
      text
      likes
      dislikes
      replies {
        id
        text
        likes
        dislikes
      }
    }
  }
`

export const ADD_DISLIKE_MESSAGE = gql `
  mutation PostMutation($messageId: ID!, $dislike: Boolean!) {
    addDislikeMessage(messageId: $messageId, dislike: $dislike) {
      id
      text
      likes
      dislikes
      replies {
        id
        text
        likes
        dislikes
      }
    }
  }
`

export const ADD_LIKE_REPLY = gql `
  mutation PostMutation($replyId: ID!, $like: Boolean!) {
    addLikeReply(replyId: $replyId, like: $like) {
      id
      text
      likes
      dislikes
    }
  }
`

export const ADD_DISLIKE_REPLY = gql `
  mutation PostMutation($replyId: ID!, $dislike: Boolean!) {
    addDislikeReply(replyId: $replyId, dislike: $dislike) {
      id
      text
      likes
      dislikes
    }
  }
`